// soal looping pertama 
console.log( 'LOOPING PERTAMA');
for ( var deret = 2; deret < 22; deret += 2) {
    console.log ( deret + " - I love coding")
}

console.log('-----------------------------------------');

console.log( 'LOOPING KEDUA');
for(var deret = 20; deret > 0; deret -= 2) {
    console.log( deret + " - I will become a mobile developer")
}

console.log('-----------------------------------------');
//soal Looping kedua  (menggunakan for)


for( var angka = 1; angka < 21; angka++) {

if ( angka%2==0) {
    console.log(angka + "- Berkualitas")
} else if ( angka%3==0 ) {
    console.log(angka + "- I Love Coding ")
} else {
    console.log(angka +"- Santai")
}
}

console.log('-----------------------------------------');
var baris = 4;

for (var i = 0; i < baris; i++) {
	var cetakSimbol = '';

	for (var j = 0; j < 8; j++) {
		if (i % 2 === 0) {
			cetakSimbol += '#';
		} else {
			cetakSimbol += ''+ '#';
		}
	}
	console.log(cetakSimbol);
}

console.log('-----------------------------------------');
// Soal nomor 4 Membuat Tangga

var baris = 7;
for (var i = 1; i <= baris; i++) {
	var cetakSimbol = '#';
	for (var j = 2; j <= i; j++) {	
    cetakSimbol += "#";
	}
	console.log(' ' + cetakSimbol);
}

console.log('-----------------------------------------');
// Soal nomor 5 Membuat Papan Catur

var baris = 8;

for (var i = 1; i <= baris; i++) {
   		if (i % 2 === 0) {
            console.log("# # # # ")
		} else {
			console.log("  "+"# # # #")
        } 
    }