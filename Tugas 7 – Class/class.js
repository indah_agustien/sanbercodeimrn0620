console.log("========================**animal class // Release 0**========================");

var legs = 4;
class Animal {
    
    constructor(name) {
        this.name = name;
        this.cold_blooded = "false"
       
    }
    
    get legs() {
        legs= 4;
        return legs;
    }
}
var sheep = new Animal("shaun");
console.log(sheep.name) 
console.log(sheep.legs) 
console.log(sheep.cold_blooded) // false

console.log("========================**animal class // Release 1**========================");

// Code class Ape dan class Frog di sini

class Ape extends Animal {
    yell() {
console.log("Auooo")
    }
}
 
class Frog extends Animal {
    jump() {
console.log("hop hop")
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


console.log("========================**Function to Class**========================");

class Clock  {
    constructor({template}) {
       this.output= template.replace('h' , this.hours).replace('m', this.mins).replace('s', this.secs);
    }

     render() {
    
        var date = new Date();
    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;
      
      console.log(clock.output);
    }
    
  stop () {
    clearInterval(timer);
  };

  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();